#!/bin/bash

{{/*
Copyright 2019 Samsung Electronics Co., Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/}}

set -ex

HM_PORT_MAC=$(cat /tmp/pod-shared/HM_PORT_MAC)
HM_NETWORK_ID=$(cat /tmp/pod-shared/HM_NETWORK_ID)

BRNAME=brq$(echo $HM_NETWORK_ID|cut -c 1-11)

ip link set dev $BRNAME address $HM_PORT_MAC

iptables -I INPUT -i $BRNAME -p udp --dport {{ .Values.conf.octavia.health_manager.bind_port }} -j ACCEPT

cat > /tmp/dhclient.conf <<EOF
  request subnet-mask,broadcast-address,interface-mtu;
  do-forward-updates false;
EOF

dhclient -v $BRNAME -cf /tmp/dhclient.conf
